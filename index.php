<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('core/Route.php');
require_once ('core/Model.php');
require_once ('core/View.php');
require_once ('core/Controller.php');

Route::start();