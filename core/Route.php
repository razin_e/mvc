<?php

class Route{

    static function start(){
        $controllerName = 'Main';
        $actionName = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);


        if (!empty($routes[1])){
            $controllerName = $routes[1];
        }

        if (!empty($routes[2])){
            $actionName = $routes[2];
        }

        //добавляем префиксы

        $modelName = 'Model_'. $controllerName;
        $controllerName = 'Controller_'. $controllerName;
        $actionName = 'action_'. $actionName;

        $modelFile = $modelName . '.php';

        if (file_exists('models/'.$modelFile)){
            include 'models/' . $modelFile;
        }

        $controllerFile = $controllerName. '.php';

        if (file_exists('controllers/' . $controllerFile)){
            include 'controllers/' . $controllerFile;
        } else {
            Route::ErrorPage404();
        }


        $controller = new $controllerName;
        $action = $actionName;

        if (method_exists($controller,$action)){
            $controller->$action();
        }  else {
            Route::ErrorPage404();
        }
    }


    static public function ErrorPage404(){
        echo '<b>Ошибка 404.</b><br>Page not found!';
        die();
    }

}




















