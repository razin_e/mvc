<?php

class Model_Main extends Model{

    public function getContacts(){
        $contacts = [
            'phone' => '123-321-123',
            'address' => 'Odessa city, Kanatna street',
            'email' => 'test@email.com'
        ];

        return $contacts;
    }
}