<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Home page</title>
    <style>
        div{
            text-align: center;
            margin: auto 0;
        }
        ul{
            list-style-type: none;
        }
    </style>
</head>
<body>
    <div id="header">
        <ul>
            <li>
                <a href="/">Home page</a>
                <a href="/main/contacts">Contacts</a>
            </li>
        </ul>
        <hr><br>
    </div>

    <?php

    include 'views/'.$content_view; ?>



    <div id="footer">
        <br>
        <hr>
        <p>
            <i>Hillel student Homepage 2017</i>
        </p>
    </div>
</body>
</html>